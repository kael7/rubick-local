#coding=utf-8
import urllib2
import os
import gzip
import StringIO
import shutil
import json
import time
from multiprocessing.dummy import Pool as ThreadPool
from bs4 import BeautifulSoup
from whoosh.index import create_in
from whoosh.fields import *
from whoosh.qparser import QueryParser
from jieba.analyse import ChineseAnalyzer
from gum import total_count, analyze_every_year, analyze_every_mouth_count, analyze_every_year_count
global LOCAL_CACHE
LOCAL_CACHE = {}


URL = "http://www.mutongzixun.com/index.php?m=content&c=index&a=lists&catid=8"
HOME_URL = "http://www.mutongzixun.com"
POOLSIZE = 10

default_encoding = "utf-8"
if default_encoding != sys.getdefaultencoding():
    reload(sys)
    sys.setdefaultencoding(default_encoding)
analyzer = ChineseAnalyzer()
schema = Schema(title=TEXT(stored=True), path=ID(stored=True), content=TEXT(stored=True, analyzer=analyzer))
indexdir = os.path.join(os.path.dirname(__file__), "indexdir")
if os.path.exists(indexdir) is False:
    os.mkdir(indexdir)
idx = create_in("indexdir", schema)
writer = idx.writer()


headers = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate, compress',
    'Accept-Language': 'en-us;q=0.5,en;q=0.3',
    'Cache-Control': 'max-age=0',
    'Connection': 'keep-alive',
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:22.0) Gecko/20100101 Firefox/22.0'
}


css_style = "<style style='text/css'>b{color:black;background-color:red}</style>"


def get_resource_path():
    path = os.path.dirname(__file__) + '/resource/'
    if os.path.exists(path) is False:
        os.mkdir(path)
    return path


def get_result_path():
    path = os.path.dirname(__file__) + '/result/'
    if os.path.exists(path) is False:
        os.mkdir(path)
    return path


def get_html(URL):
    request = urllib2.Request(URL, headers=headers)
    response = urllib2.urlopen(request, timeout=30)
    data = response.read()
    try:
        data = gzip.GzipFile(fileobj=StringIO.StringIO(data), mode="r").read()
    except Exception, e:
        pass
    return data


def get_detail_html(html):
    def filter_span_blank(str):
        patter = re.compile("<span></span>")
        if patter.search(str.encode("gbk")):
            return False
        else:
            return True

    def bink_span(str1, str2):
        return str(str1) + str(str2)
    result_span = reduce(bink_span, filter(filter_span_blank, BeautifulSoup(html, "html.parser").find_all('span')), "")
    return result_span


def save_resource_file(link):
    try:
        filename = str("%s_%s_%s.html" % (re.search("(\d+-\d+-\d+)", link['time']).groups()[0], link['title'], link['id']))
        html = get_html(link['link'])
        content = get_detail_html(html)
        print transfer_decode("Begin save %s resouce html" % filename)
        # print os.path.join(os.path.dirname(__file__) + '/resource/' + filename)
        with open(unicode(get_resource_path() + filename, 'utf-8'), 'w') as f:
            f.write(link['time'] + "<br>" + link['link'] + "<br>" + content)

    except Exception, e:
        print str(e)


def get_summary_content_link(html):
    soups = BeautifulSoup(html, "html.parser")
    link_pattern = re.compile('href="(.*)" rel="bookmark"')
    title_pattern = re.compile('rel="bookmark">(.*)</a><!')
    time_pattern = re.compile('>(.*)</span>')
    id_pattern = re.compile('&id=(\d+)')
    result = []

    for each_content in soups.find_all('div', attrs={'class': 'content'}):
        new_html = str(each_content)
        h2_html = BeautifulSoup(new_html, "html.parser").find_all('h2')[0]
        time_html = BeautifulSoup(new_html, "html.parser").find_all('span', attrs={'class': 'left_author_span'})[0]
        link_a = HOME_URL + link_pattern.search(str(h2_html)).groups()[0].replace("amp;", "")
        title_text = str(title_pattern.search(str(h2_html)).groups()[0])
        time = str(time_pattern.search(str(time_html)).groups()[0])
        id = id_pattern.search(link_a).groups()[0]
        content_links = {
            "title": title_text,
            "time": time,
            'link': link_a,
            'id': id
        }
        result.append(
                content_links
            )
    return result


def create_links():
    links = [URL]
    page_range = get_page()
    for each in page_range:
        new_link = URL + "&page=" + str(each)
        links.append(new_link)
    return links


def get_content_links_task(url):
    print transfer_decode("Begin collect %s content link" % url)
    result = get_summary_content_link(get_html(url))
    return result


def get_content_links_main():
    content_links = []
    try:

        results = []
        link_pool = ThreadPool(50)
        for each_link in create_links():
            results.append(link_pool.apply_async(get_content_links_task, (each_link,)))
        for i in results:
            i.wait()
            content_links.extend(i.get())
        link_pool.close()
        link_pool.join()
        return content_links

    except Exception, e:
        print str(e)
        return content_links


def is_download_link_content(content_link):
    global LOCAL_CACHE
    latest_time = LOCAL_CACHE["latest_time"]
    is_update_latest = get_config()['is_update_latest_resources']
    if is_update_latest == 1:
        if compare_two_time(content_link['time'], latest_time):
            return True
        else:
            return False
    else:
        return True


def get_content_main():
        link_pool = ThreadPool(50)
        contents_links = get_content_links_main()
        for each_link in contents_links:
            # if is_update_latest == 1:
            #     if compare_two_time(each_link['time'], tmp_time):
            #         tmp_time = each_link['time']
            #     if compare_two_time(latest_time, each_link['time']):
            #         continue
            if is_download_link_content(each_link):
                link_pool.apply_async(save_resource_file, (each_link,))
            else:
                break

        global LOCAL_CACHE
        LOCAL_CACHE['latest_time'] = contents_links[0]['time']

        link_pool.close()
        link_pool.join()


def insert_query_db_main(config):
    try:
        link_pool = ThreadPool(20)
        for each_resource in os.listdir(get_resource_path().decode("utf-8")):
            if main_filter(each_resource, config) is False:
                continue
            load_resources(each_resource)
        link_pool.close()
        link_pool.join()

    except Exception, e:
        print(str(e))


def load_resources(each_resource):
    try:
        filepath = os.path.join(get_resource_path(), each_resource)
        with open(filepath, 'r') as f:
            content = f.read()
        print transfer_decode("Begin %s into local resources" % each_resource)
        writer.add_document(
            title=each_resource,
            path=filepath,
            content=content.decode('utf-8')
        )
    except Exception, e:
        print str(e)


def search_key_word(key_word):

    searcher = idx.searcher()
    parser = QueryParser("content", schema=idx.schema)
    q = parser.parse(key_word)
    results = searcher.search(q, limit=99999, collapse_limit=5)
    results.fragmenter.charlimit = None
    results.fragmenter.maxchars = 99999
    results.fragmenter.surround = 99999
    return_result = []
    for hit in results:
        return_result.append(
            {
                "title": hit['title'],
                "path": hit['path'],
                "content": decorate_into_html(hit.highlights("content", top=99999))
            }
        )
    return return_result

    # for hit in results:
    #     filepath = os.path.join(get_resource_path(), hit['path'])
    #     print(hit.highlights("content", top=99999))


def decorate_into_html(results):
    results = str(results).replace("&lt;", "<")
    results = str(results).replace("&gt;", ">")
    return results


def save_results(results):
    print "Begin Save Results!"
    result_path = get_result_path()
    if os.path.exists(result_path) is False:
        os.mkdir(result_path)
    shutil.rmtree(result_path)
    os.mkdir(result_path)
    for each in results:
        filename = each['title']
        filepath = os.path.join(result_path, filename)
        with open(filepath, "w") as f:
            f.write(css_style + str(each["content"]))


def main_filter(filename, configuration):
    functions = ['filter_month', 'filter_year']
    for each in functions:
        if getattr(sys.modules[__name__], each)(filename, configuration):
            pass
        else:
            return False
    return True


def filter_month(filename, configuration):
    mouth = re.search("\d+-(\d+).*", filename).groups()[0]
    if len(configuration['search_mouth']) == 0:
        return True

    if int(mouth) in configuration["search_mouth"]:
        return True
    else:
        return False


def filter_year(filename, configuration):
    mouth = re.search("(\d+)-\d+.*", filename).groups()[0]
    if len(configuration['search_years']) == 0:
        return True
    if int(mouth) in configuration['search_years']:
        return True
    else:
        return False


def get_page():
    try:
        ALL_PAGE = int(get_all_pages_num()) + 1
        config = get_config()
        page_range = config['page_range']
        if len(page_range) == 1 and str(page_range[0]).find("*") > -1:
            return range(1, ALL_PAGE)
        if len(page_range) == 1 and str(page_range[0]).find("-") > -1:
            from_page = (page_range[0].split("-"))[0]
            to_page = (page_range[0].split("-"))[1]
            return range(int(from_page), int(to_page) + 1)
        if len(page_range) > 1:
            return page_range

    except Exception, e:
        print str(e)

    return []


def get_config():
    configuration_path = os.path.join(os.path.dirname(__file__), "configuration")
    with open(configuration_path, "r") as f:
        config = f.read()
    config = json.loads(config)
    return config


def main():
    """
    Load cache
    """
    print "Loading local cache"
    global LOCAL_CACHE
    LOCAL_CACHE = get_cache()
    """
    Load configuration
    """
    print "Loading local configuration"
    config = get_config()
    key_words = u'' + str(config['keywords']).decode('utf-8')
    is_update_resource = config['is_update_resources']
    is_search_key_word = config['is_search_key_word']

    if is_update_resource == 1:
        print transfer_decode("**************开始进行数据爬取***************")
        get_content_main()

    if is_search_key_word == 1:
        # """
        #     Load Resource into system
        # """
        for each_resource in os.listdir(get_resource_path().decode("utf-8")):
            if main_filter(each_resource, config) is False:
                continue
            filepath = os.path.join(get_resource_path(), each_resource)
            with open(filepath, 'r') as f:
                content = f.read()
            print transfer_decode("Begin %s into local resources" % each_resource)
            writer.add_document(
                title=each_resource,
                path=filepath,
                content=content.decode('utf-8')
            )
        print transfer_decode("**************开始进行数据检索***************")
        insert_query_db_main(config)
        writer.commit()

        """
        Search Key Word From System
        """

        results = search_key_word(u'' + key_words)

        """
        Save Results
        """
        save_results(results)

        "Save Cache"
        save_cache()

    "Analyze Data"
    analyze_data(config)


def transfer_decode(strs):
    return strs.decode("utf-8")


def analyze_data(config):
    print transfer_decode("**************开始进行数据解析***************")
    if config['is_all_count'] == 1:
        print ""
        print transfer_decode("#关键字在所有日期中一共出现了%s次" % str(total_count()))

    if config['is_every_mouth_union'] == 1:
        print ""
        result = analyze_every_year()
        print transfer_decode("#关键字在所有的年份中都出现的月份有 %s " % ",".join(result))

    if config['is_every_year_count'] == 1:
        print ""
        print transfer_decode("#关键字在每年中出现的次数为")
        result = analyze_every_year_count()
        for i in result:
            print transfer_decode("%s年出现了%s次" % (i, result[i]))

    if config['is_every_mouth_count'] == 1:
        print ""
        result = analyze_every_mouth_count()
        print transfer_decode("#关键字在每个月中的出现结果如下:")
        for i in result:
            print transfer_decode("%s 中出现了 %s 次" % (i, result[i]))


def get_all_pages_num():
    html = get_html(URL)
    pages_div = BeautifulSoup(html, "html.parser").find_all("div", id='pages')[0]
    return re.findall(">(\d+)</a>", str(pages_div))[-1]


def compare_two_time(time1, time2):
    return time.strptime(time1, "%Y-%m-%d %H:%M:%S") > time.strptime(time2, "%Y-%m-%d %H:%M:%S")


def defaults_cache():
    local_cache = {"latest_time": u"1990-01-01 22:00:00"}
    return local_cache


def get_cache():
    cache_path = os.path.join(os.path.dirname(__file__), "cache")
    if os.path.exists(cache_path) is False:
        return defaults_cache()

    with open(cache_path) as f:
        config = f.read().encode("utf-8")
    try:
        config_key = json.loads(config)
    except Exception, e:
        return defaults_cache()

    return config_key


def save_cache():
    global LOCAL_CACHE
    with open(os.path.join(os.path.dirname(__file__), "cache"), 'w') as f:
        f.write(json.dumps(LOCAL_CACHE))

if __name__ == "__main__":
    STAND_TIME = 30
    start_time = time.time()
    main()
    end_time = time.time()
    spend_time = str(end_time - start_time)
    print transfer_decode("检索结束，用时：%s" % spend_time)
    if end_time - start_time < 30:
        print transfer_decode("检索速度快如闪电，赏小哥一个鸡腿^_^")
    else:
        print transfer_decode("小猪肚子都等饿了，罚小哥跪键盘-_-||")
    print transfer_decode("按 Enter 键结束程序".decode())
    name = raw_input()

